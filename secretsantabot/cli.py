# -*- coding: utf-8 -*-

import asyncio
import logging

from aiogram import Bot, Dispatcher

from secretsantabot.handlers import start


def start_bot():
    log_file = '/tmp/secretsanta.log'
    bot_token = ''

    logging.basicConfig(
        filename=log_file,
        format="%(asctime)s %(name)s %(module)s:%(funcName)s:%(lineno)s - %(message)s",
        level=logging.DEBUG
    )

    loop = asyncio.get_event_loop()
    bot = Bot(token=bot_token, loop=loop)
    dispatcher = Dispatcher(bot)

    dispatcher.register_message_handler(start, ['start'])

    return 0