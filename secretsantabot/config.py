# -*- coding: utf-8 -*-

import configparser
import os
from os.path import expanduser


__CONFIG = None


def get_default_config_path():
    return expanduser('~/.config/secretsantabot/config.ini')


def create_default_config(config_dest):
    configdir = os.path.dirname(config_dest)

    if not os.path.exists(configdir):
        os.makedirs(configdir)

    if os.path.exists(config_dest):
        os.rename(config_dest, config_dest + '~')

    with open(config_dest, "w") as f:
        f.write("""[general]
log=~/.local/share/nursejoy/log/debug.log
language=es
[telegram]
token=###
""")

    print("Configuration file has been created on «{}».\nPlease check "
          "the configuration and change it as your wishes.".format(
              config_dest))


def get_config(config_path=None):
    global __CONFIG

    if __CONFIG is not None:
        return __CONFIG

    if not config_path:
        print('Error: no configuration path is provided')
        return None

    if not os.path.isfile(config_path):
        return None

    __CONFIG = configparser.ConfigParser()
    __CONFIG.read(config_path)

    return __CONFIG
